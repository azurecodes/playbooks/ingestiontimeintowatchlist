# IngestionTimeIntoWatchlist
under testing

This playbook was created for this blogpost:

## Prequisities
Create a watchlist in your Sentinel. Its name is going to be used in the Playbook.

The Watchlist needs two columns:
* DataTable: To store the name of the Data Tables.
* TimeIngestionDelay: To store the ingestion delay for each Data Table.

## Install
The Playbook can be deployed as a template.

1. Open https://portal.azure.com/#create/Microsoft.Template
1. Click " Build your own template in the editor"
1. Copy the content of the azuredeploy.json file into the textbox.
1. Define the variables. Custom ones:
    1. Playbook Name: trivial
    1. Watchlist Name: The name of the watchlist you created in the prequisities section
    1. LAW Name: Log Analytics Worskpace (Sentinel) name you created your watchlist in.
    1. Connection_azuremonitor: Name of the API Connection 
    1. Connection_azuresentinel: Name of the API Connection
1. After deployment you have to authorize the API Connections
    1. Open the Playbook
    1. Choose API Connections option
    1. Choose the connection you want to modify
    1. Click Edit API Connection
    1. Then 'Authorize' and Save It


You don't have to use the authorized connections, you can create your own or use other methods.

The Playbook is Disabled by default.

Some values are defined based on the values you used in the Template Parameters page. Like the Subscription you used to deploy the playbook is the same one the script uses the query a workspace, or to find the watchlist. If you want to change these values you can do it in the code of the Playbook after deployment.
